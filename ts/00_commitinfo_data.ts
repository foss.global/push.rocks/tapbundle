/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/tapbundle',
  version: '5.0.13',
  description: 'tap bundled for tapbuffer'
}
